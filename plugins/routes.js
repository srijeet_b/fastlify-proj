
function routers (fastify, options, done) {
  fastify.get('/', (req, reply) => {
    return reply.code(200).send({ message: 'welcome, please visit  "/g" endpoint to go to google.com'});
  });
  
  fastify.get('/:redirector', (req, reply) => {
    return reply.code(303).redirect('https://google.com');
  });

  done();
}

module.exports = routers;