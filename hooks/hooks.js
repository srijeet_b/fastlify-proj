function hooks (fastify) {
  fastify.addHook('onRequest', (req ,reply, done) => {
    console.log(req.params);
    if (Object.keys(req.params).length === 0 || (req.params.redirector && typeof(req.params.redirector) === 'string' && req.params.redirector === 'g')) {
      done();
    }
    return reply.code(400).send({ message: 'This route path is invalid !'});
  });
}

module.exports = hooks;