const hooks = require('./hooks/hooks');

const fastify = require('fastify')({
  logger: true
});

hooks(fastify);
fastify.register(require('./plugins/routes'));

fastify.listen(3001, function (err, address) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${address}`)
})