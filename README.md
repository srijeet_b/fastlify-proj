# Fastlify - Redirector 

## How to use the app

```
1. Open a terminal and run 
> git clone https://srijeet_b@bitbucket.org/srijeet_b/fastlify-proj.git

2. cd into fastilify-proj

3. This package uses Yarn so in terminal run
> yarn install

4. After the application install completes, run
> yarn start

5. We will have the app running on port 3031, open a browser and go to 
>  http://localhost:3031

```